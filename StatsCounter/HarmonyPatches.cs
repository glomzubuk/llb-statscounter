﻿using System;
using HarmonyLib;
using LLHandlers;

namespace StatsCounter
{
    public static class DiplayCounterInMainMenu
    {
        [HarmonyPatch(typeof(ScreenMenu), nameof(ScreenMenu.UpdatePlayerInfo))]
        [HarmonyPostfix]
        public static void OnOpen_Postfix(ScreenMenu __instance)
        {
            string lbLevel = TextHandler.Get("MAIN_TXT_MATCHES", new string[] {
                BDCINPKBMBL.matchesPlayed.ToString()
            }) + "\n";

            lbLevel += $"Win streak: {StatsCounter.GetCurrentWinStreak()?.ToString() ?? "-"}\n";

            if (StatsCounter.displayBest.Value)
            {
                lbLevel += $"Best streak: {StatsCounter.currentBestWinStreak.Value}\n";
            }
            else
            {
                lbLevel += TextHandler.Get("MAIN_TXT_LEVEL", new string[] {
                    __instance.playerLevel
                });
            }
            TextHandler.SetText(__instance.lbLevel, lbLevel);
        }

        [HarmonyPatch(typeof(KKMGLMJABKH), nameof(KKMGLMJABKH.AOBECLEDAPM))]
        [HarmonyPostfix]
        public static void Init_Postfix(KKMGLMJABKH __instance) // PlatformSteam??
        {
            __instance.LHGHOTOTO.KGMAJPFBNNJ += StatsCounter.Handle_StatsInitialized;
        }
    }
}
