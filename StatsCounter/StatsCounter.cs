﻿using System;
using UnityEngine;
using Steamworks;
using CodeStage.AntiCheat.ObscuredTypes;
using LLScreen;
using LLGUI;
using LLHandlers;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using LLBML;
using LLBML.Players;
using StatsCounter.Platforms;

namespace StatsCounter
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInProcess("LLBlaze.exe")]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    public class StatsCounter : BaseUnityPlugin
    {
        public static StatsCounter Instance { get; private set; } = null;
        public static ManualLogSource Log { get; private set; } = null;

        public static ConfigEntry<bool> displayBest;
        public static ConfigEntry<long> currentBestWinStreak;

        void Awake()
        {
            Instance = this;
            Log = this.Logger;


            Logger.LogInfo("Hello, world!");

            displayBest = Config.Bind("Toggles", "DisplayBest", true, "Display the best streak in the main menu");

            currentBestWinStreak = Config.Bind("Toggles", "CurrentBestWinStreak", 0L,
                new ConfigDescription("Internal best counter", null, "modmenu_hidden"));

            var harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
            harmoInstance.PatchAll(typeof(DiplayCounterInMainMenu));
        }


        void Start()
        {
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info);
            //Platform.Stats.Unwrap().KGMAJPFBNNJ += 
        }

        void Update()
        {
            if (ScreenApi.CurrentScreens[0] is ScreenMenu screenMenu)
            {
                if (displayBest.Value && screenMenu.lbVersion.transform.gameObject.activeInHierarchy == true)
                {
                    screenMenu.lbVersion.transform.gameObject.SetActive(false);
                }
            }

            if (ScreenApi.CurrentScreens[0] is ScreenPlayers screenPlayers)
            {
                UpdateWinStreakButtons(screenPlayers);
            }
            else
            {
                for (int i = 0; i < winStreakButtons.Length; i++)
                {
                    if (winStreakButtons[i] != null)
                    {
                        Destroy(winStreakButtons[i]);
                        winStreakButtons[i] = null;
                    }
                }
            }
        }

        LLButton[] winStreakButtons = new LLButton[4];
        string[] winStreakValue = new string[4];
        public void UpdateWinStreakButtons(ScreenPlayers screenPlayers)
        {
            for (int i = 0; i < screenPlayers.playerSelections.Length; i++)
            {
                Player player = Player.GetPlayer(i);
                if (player?.peer != null)
                {
                    if (winStreakButtons[player.nr] == null)
                    {
                        //Logger.LogDebug("Test 3");
                        PlayersSelection playerSelection = screenPlayers.playerSelections[i];
                        LLButton btPlayerName = playerSelection.btPlayerName;
                        winStreakButtons[player.nr] = Instantiate(btPlayerName);
                        winStreakButtons[player.nr].transform.SetParent(btPlayerName.transform.parent);
                        winStreakButtons[player.nr].transform.localPosition = btPlayerName.transform.localPosition;
                        winStreakButtons[player.nr].transform.localPosition += new Vector3(0, -54, 0);
                        winStreakButtons[player.nr].transform.localScale = new Vector3(1, 1, 1);
                        winStreakButtons[player.nr].onClick = (playerNr) => {
                            if (UnityEngine.Random.Range(0, 1000) == 0)
                            {
                                AudioHandler.PlaySfx(Sfx.PARRY_COUNTER);
                            }
                        };


                        //Logger.LogDebug($"Test 1 peerid {player.peer.peerId}");
                        string streak = "-";
                        int localPlayerNr = Player.GetLocalPlayer()?.peer?.playerNr ?? -1;
                        Logger.LogDebug($" player nr: {player.nr} | lpnr: {localPlayerNr}");
                        if (player.nr == localPlayerNr)
                        {

                            streak = GetCurrentWinStreak()?.ToString() ?? "-";
                        }
                        else
                        {
                            //Logger.LogDebug("Test 5 bis");
                            streak = "-";
                            //streak = GetUserCurrentWinStreak(player.peer.peerId)?.ToString() ?? "-";
                        }
                        //Logger.LogDebug($"streak: {streak}");
                        winStreakButtons[player.nr].SetText($"Current streak: {streak}");
                    }
                }
            }

            /*
            Player.ForAll((Player player) => {
                if (player != null && screenPlayer?.playerSelections?[player.nr] != null)
                {
                    Logger.LogDebug($"player nr: {player.nr}");
                    var btPlayerName = screenPlayer.playerSelections[player.nr].btPlayerName;
                    Logger.LogDebug("Test 2");
                    if (winStreakButtons[player.nr] == null)
                    {
                        Logger.LogDebug("Test 3");
                        winStreakButtons[player.nr] = Instantiate(btPlayerName);
                        winStreakButtons[player.nr].transform.position += new Vector3(0, -10, 0);
                    }
                    if (player.nr == Player.LocalPlayerNumber)
                    {
                        Logger.LogDebug("Test 4");
                        string streak = GetCurrentWinStreak().ToString() ?? "-";
                        Logger.LogDebug($"streak: {streak}");
                        winStreakButtons[player.nr].SetText($"Current streak: {streak}");
                    }
                    else
                    {

                        Logger.LogDebug("Test 4 bis");
                        if (player.peer != null)
                        {
                            Logger.LogDebug("Test 4 bis");
                            string streak = GetUserCurrentWinStreak(player.peer.peerId).ToString() ?? "-";
                            Logger.LogDebug($"streak: {streak}");
                            winStreakButtons[player.nr].SetText($"Current streak: {streak}");
                        }
                    }
                }
            });*/
        }

        public static void Handle_StatsInitialized(bool success)
        {
            Log.LogDebug("Updating Stats? " + success);
            if (success && ScreenApi.CurrentScreens[0] is ScreenMenu screenMenu)
            {
                screenMenu.UpdatePlayerInfo();
            }
        }

        public static void UpdateBestStreak(long curStreak)
        {
            if (curStreak > currentBestWinStreak.Value)
            {
                currentBestWinStreak.Value = curStreak;
            }
        }

        public static long? GetCurrentWinStreak()
        {
            long? streak = null;
            if (Stats.Initialized)
            {
                long winStreak = Stats.Instance.GetStatValue(StatID.currentVictoryStreak);
                long lossStreak = Stats.Instance.GetStatValue(StatID.lossStreak);
                streak = winStreak - lossStreak;
            }
            if (streak != null)
            {
                UpdateBestStreak(streak.Value);
            }
            return streak;
        }
        public static long? GetUserCurrentWinStreak(string userID)
        {
            long? streak = null;
            Stats stats = Stats.Instance;
            if (Stats.Initialized && stats.state == Stats.State.ready)
            {
                if (!stats.opponentsStatsCache.ContainsKey(userID) && stats.state == Stats.State.ready)
                {
                    stats.DownloadStatsForOpponent(userID);
                    return streak;
                }

                long winStreak = stats.GetStatValueForUser(userID, StatID.currentVictoryStreak);
                long lossStreak = stats.GetStatValueForUser(userID, StatID.lossStreak);
                streak = winStreak - lossStreak;
            }
            return streak;
        }
        public static long? GetHighestWinStreak()
        {
            long? streak = null;
            if (Stats.Initialized)
            {
                long winStreak = Stats.Instance.GetStatValue(StatID.highestVictoryStreak);
            }
            return streak;
        }
    }
}
