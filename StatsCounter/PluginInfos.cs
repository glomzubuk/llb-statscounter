﻿using System.Reflection;
using StatsCounter;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfos.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfos.PLUGIN_NAME + " (" + PluginInfos.PLUGIN_ID + ")")]
[assembly: AssemblyProduct(PluginInfos.PLUGIN_NAME)]
#endregion

namespace StatsCounter
{
    internal static class PluginInfos
    {
        public const string PLUGIN_NAME = "StatsCounter";
        public const string PLUGIN_ID = "fr.glomzubuk.plugins.llb.statscounter";
        public const string PLUGIN_VERSION = "1.1.4";
    }
}