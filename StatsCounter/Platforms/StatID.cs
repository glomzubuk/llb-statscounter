﻿using System;
using LLBML.Utils;

namespace StatsCounter.Platforms
{
    public class StatID : EnumWrapper<AKFAKLAWWAE>
    {
#pragma warning disable 1591
        StatID(int id) : base(id) { }
        StatID(AKFAKLAWWAE e) : base((int)e) { }

        public override string ToString() => ((Enum)this.id).ToString();


        public static implicit operator AKFAKLAWWAE(StatID ew) => (AKFAKLAWWAE)ew.id;
        public static implicit operator StatID(AKFAKLAWWAE val) => new StatID(val);
        public static implicit operator StatID(int id) => new StatID(id);
        public static implicit operator StatID(Enum val) => new StatID((int)val);
        public static implicit operator Enum(StatID ew) => (Enum)ew.id;

        #region wrapper
        public enum Enum
        {
            none = -1,
            numMatchesPlayed,
            numMatchesWon,
            numMatchesLost,
            currentVictoryStreak,
            highestVictoryStreak,
            numMatchesPlayed_KID,
            numMatchesPlayed_ROBOT,
            numMatchesPlayed_CANDY,
            numMatchesPlayed_BOOM,
            numMatchesPlayed_CROC,
            numMatchesPlayed_PONG,
            numMatchesPlayed_BOSS,
            numMatchesPlayed_COP,
            numMatchesPlayed_ELECTRO,
            numMatchesPlayed_SKATE,
            numMatchesPlayed_RANDOM,
            numMatchesWon_KID,
            numMatchesWon_ROBOT,
            numMatchesWon_CANDY,
            numMatchesWon_BOOM,
            numMatchesWon_CROC,
            numMatchesWon_PONG,
            numMatchesWon_BOSS,
            numMatchesWon_COP,
            numMatchesWon_ELECTRO,
            numMatchesWon_SKATE,
            numMatchesWon_RANDOM,
            numMatchesLost_KID,
            numMatchesLost_ROBOT,
            numMatchesLost_CANDY,
            numMatchesLost_BOOM,
            numMatchesLost_CROC,
            numMatchesLost_PONG,
            numMatchesLost_BOSS,
            numMatchesLost_COP,
            numMatchesLost_ELECTRO,
            numMatchesLost_SKATE,
            numMatchesLost_RANDOM,
            lastMatchCompleted,
            numOfDisconnects,
            global_NumMatchesPlayed,
            global_NumMatchesPlayed_KID,
            global_NumMatchesPlayed_ROBOT,
            global_NumMatchesPlayed_CANDY,
            global_NumMatchesPlayed_BOOM,
            global_NumMatchesPlayed_CROC,
            global_NumMatchesPlayed_PONG,
            global_NumMatchesPlayed_BOSS,
            global_NumMatchesPlayed_COP,
            global_NumMatchesPlayed_ELECTRO,
            global_NumMatchesPlayed_SKATE,
            global_NumMatchesPlayed_RANDOM,
            global_NumMatchesWon_KID,
            global_NumMatchesWon_ROBOT,
            global_NumMatchesWon_CANDY,
            global_NumMatchesWon_BOOM,
            global_NumMatchesWon_CROC,
            global_NumMatchesWon_PONG,
            global_NumMatchesWon_BOSS,
            global_NumMatchesWon_COP,
            global_NumMatchesWon_ELECTRO,
            global_NumMatchesWon_SKATE,
            global_NumMatchesLost_KID,
            global_NumMatchesLost_ROBOT,
            global_NumMatchesLost_CANDY,
            global_NumMatchesLost_BOOM,
            global_NumMatchesLost_CROC,
            global_NumMatchesLost_PONG,
            global_NumMatchesLost_BOSS,
            global_NumMatchesLost_COP,
            global_NumMatchesLost_ELECTRO,
            global_NumMatchesLost_SKATE,
            lossStreak,
            numMatchesPlayed_GRAF,
            numMatchesWon_GRAF,
            numMatchesLost_GRAF,
            global_NumMatchesPlayed_GRAF,
            global_NumMatchesWon_GRAF,
            global_NumMatchesLost_GRAF,
            numMatchesPlayed_BAG,
            numMatchesWon_BAG,
            numMatchesLost_BAG,
            global_NumMatchesPlayed_BAG,
            global_NumMatchesWon_BAG,
            global_NumMatchesLost_BAG,
            max
        }

        public enum Enum_Mapping
        {
            none = AKFAKLAWWAE.none,
            numMatchesPlayed = AKFAKLAWWAE.numMatchesPlayed,
            numMatchesWon = AKFAKLAWWAE.numMatchesWon,
            numMatchesLost = AKFAKLAWWAE.numMatchesLost,
            currentVictoryStreak = AKFAKLAWWAE.currentVictoryStreak,
            highestVictoryStreak = AKFAKLAWWAE.highestVictoryStreak,
            numMatchesPlayed_KID = AKFAKLAWWAE.numMatchesPlayed_KID,
            numMatchesPlayed_ROBOT = AKFAKLAWWAE.numMatchesPlayed_ROBOT,
            numMatchesPlayed_CANDY = AKFAKLAWWAE.numMatchesPlayed_CANDY,
            numMatchesPlayed_BOOM = AKFAKLAWWAE.numMatchesPlayed_BOOM,
            numMatchesPlayed_CROC = AKFAKLAWWAE.numMatchesPlayed_CROC,
            numMatchesPlayed_PONG = AKFAKLAWWAE.numMatchesPlayed_PONG,
            numMatchesPlayed_BOSS = AKFAKLAWWAE.numMatchesPlayed_BOSS,
            numMatchesPlayed_COP = AKFAKLAWWAE.numMatchesPlayed_COP,
            numMatchesPlayed_ELECTRO = AKFAKLAWWAE.numMatchesPlayed_ELECTRO,
            numMatchesPlayed_SKATE = AKFAKLAWWAE.numMatchesPlayed_SKATE,
            numMatchesPlayed_RANDOM = AKFAKLAWWAE.numMatchesPlayed_RANDOM,
            numMatchesWon_KID = AKFAKLAWWAE.numMatchesWon_KID,
            numMatchesWon_ROBOT = AKFAKLAWWAE.numMatchesWon_ROBOT,
            numMatchesWon_CANDY = AKFAKLAWWAE.numMatchesWon_CANDY,
            numMatchesWon_BOOM = AKFAKLAWWAE.numMatchesWon_BOOM,
            numMatchesWon_CROC = AKFAKLAWWAE.numMatchesWon_CROC,
            numMatchesWon_PONG = AKFAKLAWWAE.numMatchesWon_PONG,
            numMatchesWon_BOSS = AKFAKLAWWAE.numMatchesWon_BOSS,
            numMatchesWon_COP = AKFAKLAWWAE.numMatchesWon_COP,
            numMatchesWon_ELECTRO = AKFAKLAWWAE.numMatchesWon_ELECTRO,
            numMatchesWon_SKATE = AKFAKLAWWAE.numMatchesWon_SKATE,
            numMatchesWon_RANDOM = AKFAKLAWWAE.numMatchesWon_RANDOM,
            numMatchesLost_KID = AKFAKLAWWAE.numMatchesLost_KID,
            numMatchesLost_ROBOT = AKFAKLAWWAE.numMatchesLost_ROBOT,
            numMatchesLost_CANDY = AKFAKLAWWAE.numMatchesLost_CANDY,
            numMatchesLost_BOOM = AKFAKLAWWAE.numMatchesLost_BOOM,
            numMatchesLost_CROC = AKFAKLAWWAE.numMatchesLost_CROC,
            numMatchesLost_PONG = AKFAKLAWWAE.numMatchesLost_PONG,
            numMatchesLost_BOSS = AKFAKLAWWAE.numMatchesLost_BOSS,
            numMatchesLost_COP = AKFAKLAWWAE.numMatchesLost_COP,
            numMatchesLost_ELECTRO = AKFAKLAWWAE.numMatchesLost_ELECTRO,
            numMatchesLost_SKATE = AKFAKLAWWAE.numMatchesLost_SKATE,
            numMatchesLost_RANDOM = AKFAKLAWWAE.numMatchesLost_RANDOM,
            lastMatchCompleted = AKFAKLAWWAE.lastMatchCompleted,
            numOfDisconnects = AKFAKLAWWAE.numOfDisconnects,
            global_NumMatchesPlayed = AKFAKLAWWAE.global_NumMatchesPlayed,
            global_NumMatchesPlayed_KID = AKFAKLAWWAE.global_NumMatchesPlayed_KID,
            global_NumMatchesPlayed_ROBOT = AKFAKLAWWAE.global_NumMatchesPlayed_ROBOT,
            global_NumMatchesPlayed_CANDY = AKFAKLAWWAE.global_NumMatchesPlayed_CANDY,
            global_NumMatchesPlayed_BOOM = AKFAKLAWWAE.global_NumMatchesPlayed_BOOM,
            global_NumMatchesPlayed_CROC = AKFAKLAWWAE.global_NumMatchesPlayed_CROC,
            global_NumMatchesPlayed_PONG = AKFAKLAWWAE.global_NumMatchesPlayed_PONG,
            global_NumMatchesPlayed_BOSS = AKFAKLAWWAE.global_NumMatchesPlayed_BOSS,
            global_NumMatchesPlayed_COP = AKFAKLAWWAE.global_NumMatchesPlayed_COP,
            global_NumMatchesPlayed_ELECTRO = AKFAKLAWWAE.global_NumMatchesPlayed_ELECTRO,
            global_NumMatchesPlayed_SKATE = AKFAKLAWWAE.global_NumMatchesPlayed_SKATE,
            global_NumMatchesPlayed_RANDOM = AKFAKLAWWAE.global_NumMatchesPlayed_RANDOM,
            global_NumMatchesWon_KID = AKFAKLAWWAE.global_NumMatchesWon_KID,
            global_NumMatchesWon_ROBOT = AKFAKLAWWAE.global_NumMatchesWon_ROBOT,
            global_NumMatchesWon_CANDY = AKFAKLAWWAE.global_NumMatchesWon_CANDY,
            global_NumMatchesWon_BOOM = AKFAKLAWWAE.global_NumMatchesWon_BOOM,
            global_NumMatchesWon_CROC = AKFAKLAWWAE.global_NumMatchesWon_CROC,
            global_NumMatchesWon_PONG = AKFAKLAWWAE.global_NumMatchesWon_PONG,
            global_NumMatchesWon_BOSS = AKFAKLAWWAE.global_NumMatchesWon_BOSS,
            global_NumMatchesWon_COP = AKFAKLAWWAE.global_NumMatchesWon_COP,
            global_NumMatchesWon_ELECTRO = AKFAKLAWWAE.global_NumMatchesWon_ELECTRO,
            global_NumMatchesWon_SKATE = AKFAKLAWWAE.global_NumMatchesWon_SKATE,
            global_NumMatchesLost_KID = AKFAKLAWWAE.global_NumMatchesLost_KID,
            global_NumMatchesLost_ROBOT = AKFAKLAWWAE.global_NumMatchesLost_ROBOT,
            global_NumMatchesLost_CANDY = AKFAKLAWWAE.global_NumMatchesLost_CANDY,
            global_NumMatchesLost_BOOM = AKFAKLAWWAE.global_NumMatchesLost_BOOM,
            global_NumMatchesLost_CROC = AKFAKLAWWAE.global_NumMatchesLost_CROC,
            global_NumMatchesLost_PONG = AKFAKLAWWAE.global_NumMatchesLost_PONG,
            global_NumMatchesLost_BOSS = AKFAKLAWWAE.global_NumMatchesLost_BOSS,
            global_NumMatchesLost_COP = AKFAKLAWWAE.global_NumMatchesLost_COP,
            global_NumMatchesLost_ELECTRO = AKFAKLAWWAE.global_NumMatchesLost_ELECTRO,
            global_NumMatchesLost_SKATE = AKFAKLAWWAE.global_NumMatchesLost_SKATE,
            lossStreak = AKFAKLAWWAE.lossStreak,
            numMatchesPlayed_GRAF = AKFAKLAWWAE.numMatchesPlayed_GRAF,
            numMatchesWon_GRAF = AKFAKLAWWAE.numMatchesWon_GRAF,
            numMatchesLost_GRAF = AKFAKLAWWAE.numMatchesLost_GRAF,
            global_NumMatchesPlayed_GRAF = AKFAKLAWWAE.global_NumMatchesPlayed_GRAF,
            global_NumMatchesWon_GRAF = AKFAKLAWWAE.global_NumMatchesWon_GRAF,
            global_NumMatchesLost_GRAF = AKFAKLAWWAE.global_NumMatchesLost_GRAF,
            numMatchesPlayed_BAG = AKFAKLAWWAE.numMatchesPlayed_BAG,
            numMatchesWon_BAG = AKFAKLAWWAE.numMatchesWon_BAG,
            numMatchesLost_BAG = AKFAKLAWWAE.numMatchesLost_BAG,
            global_NumMatchesPlayed_BAG = AKFAKLAWWAE.global_NumMatchesPlayed_BAG,
            global_NumMatchesWon_BAG = AKFAKLAWWAE.global_NumMatchesWon_BAG,
            global_NumMatchesLost_BAG = AKFAKLAWWAE.global_NumMatchesLost_BAG,
            max = AKFAKLAWWAE.max,
        }

        public static readonly StatID none = Enum.none;
        public static readonly StatID numMatchesPlayed = Enum.numMatchesPlayed;
        public static readonly StatID numMatchesWon = Enum.numMatchesWon;
        public static readonly StatID numMatchesLost = Enum.numMatchesLost;
        public static readonly StatID currentVictoryStreak = Enum.currentVictoryStreak;
        public static readonly StatID highestVictoryStreak = Enum.highestVictoryStreak;
        public static readonly StatID numMatchesPlayed_KID = Enum.numMatchesPlayed_KID;
        public static readonly StatID numMatchesPlayed_ROBOT = Enum.numMatchesPlayed_ROBOT;
        public static readonly StatID numMatchesPlayed_CANDY = Enum.numMatchesPlayed_CANDY;
        public static readonly StatID numMatchesPlayed_BOOM = Enum.numMatchesPlayed_BOOM;
        public static readonly StatID numMatchesPlayed_CROC = Enum.numMatchesPlayed_CROC;
        public static readonly StatID numMatchesPlayed_PONG = Enum.numMatchesPlayed_PONG;
        public static readonly StatID numMatchesPlayed_BOSS = Enum.numMatchesPlayed_BOSS;
        public static readonly StatID numMatchesPlayed_COP = Enum.numMatchesPlayed_COP;
        public static readonly StatID numMatchesPlayed_ELECTRO = Enum.numMatchesPlayed_ELECTRO;
        public static readonly StatID numMatchesPlayed_SKATE = Enum.numMatchesPlayed_SKATE;
        public static readonly StatID numMatchesPlayed_RANDOM = Enum.numMatchesPlayed_RANDOM;
        public static readonly StatID numMatchesWon_KID = Enum.numMatchesWon_KID;
        public static readonly StatID numMatchesWon_ROBOT = Enum.numMatchesWon_ROBOT;
        public static readonly StatID numMatchesWon_CANDY = Enum.numMatchesWon_CANDY;
        public static readonly StatID numMatchesWon_BOOM = Enum.numMatchesWon_BOOM;
        public static readonly StatID numMatchesWon_CROC = Enum.numMatchesWon_CROC;
        public static readonly StatID numMatchesWon_PONG = Enum.numMatchesWon_PONG;
        public static readonly StatID numMatchesWon_BOSS = Enum.numMatchesWon_BOSS;
        public static readonly StatID numMatchesWon_COP = Enum.numMatchesWon_COP;
        public static readonly StatID numMatchesWon_ELECTRO = Enum.numMatchesWon_ELECTRO;
        public static readonly StatID numMatchesWon_SKATE = Enum.numMatchesWon_SKATE;
        public static readonly StatID numMatchesWon_RANDOM = Enum.numMatchesWon_RANDOM;
        public static readonly StatID numMatchesLost_KID = Enum.numMatchesLost_KID;
        public static readonly StatID numMatchesLost_ROBOT = Enum.numMatchesLost_ROBOT;
        public static readonly StatID numMatchesLost_CANDY = Enum.numMatchesLost_CANDY;
        public static readonly StatID numMatchesLost_BOOM = Enum.numMatchesLost_BOOM;
        public static readonly StatID numMatchesLost_CROC = Enum.numMatchesLost_CROC;
        public static readonly StatID numMatchesLost_PONG = Enum.numMatchesLost_PONG;
        public static readonly StatID numMatchesLost_BOSS = Enum.numMatchesLost_BOSS;
        public static readonly StatID numMatchesLost_COP = Enum.numMatchesLost_COP;
        public static readonly StatID numMatchesLost_ELECTRO = Enum.numMatchesLost_ELECTRO;
        public static readonly StatID numMatchesLost_SKATE = Enum.numMatchesLost_SKATE;
        public static readonly StatID numMatchesLost_RANDOM = Enum.numMatchesLost_RANDOM;
        public static readonly StatID lastMatchCompleted = Enum.lastMatchCompleted;
        public static readonly StatID numOfDisconnects = Enum.numOfDisconnects;
        public static readonly StatID global_NumMatchesPlayed = Enum.global_NumMatchesPlayed;
        public static readonly StatID global_NumMatchesPlayed_KID = Enum.global_NumMatchesPlayed_KID;
        public static readonly StatID global_NumMatchesPlayed_ROBOT = Enum.global_NumMatchesPlayed_ROBOT;
        public static readonly StatID global_NumMatchesPlayed_CANDY = Enum.global_NumMatchesPlayed_CANDY;
        public static readonly StatID global_NumMatchesPlayed_BOOM = Enum.global_NumMatchesPlayed_BOOM;
        public static readonly StatID global_NumMatchesPlayed_CROC = Enum.global_NumMatchesPlayed_CROC;
        public static readonly StatID global_NumMatchesPlayed_PONG = Enum.global_NumMatchesPlayed_PONG;
        public static readonly StatID global_NumMatchesPlayed_BOSS = Enum.global_NumMatchesPlayed_BOSS;
        public static readonly StatID global_NumMatchesPlayed_COP = Enum.global_NumMatchesPlayed_COP;
        public static readonly StatID global_NumMatchesPlayed_ELECTRO = Enum.global_NumMatchesPlayed_ELECTRO;
        public static readonly StatID global_NumMatchesPlayed_SKATE = Enum.global_NumMatchesPlayed_SKATE;
        public static readonly StatID global_NumMatchesPlayed_RANDOM = Enum.global_NumMatchesPlayed_RANDOM;
        public static readonly StatID global_NumMatchesWon_KID = Enum.global_NumMatchesWon_KID;
        public static readonly StatID global_NumMatchesWon_ROBOT = Enum.global_NumMatchesWon_ROBOT;
        public static readonly StatID global_NumMatchesWon_CANDY = Enum.global_NumMatchesWon_CANDY;
        public static readonly StatID global_NumMatchesWon_BOOM = Enum.global_NumMatchesWon_BOOM;
        public static readonly StatID global_NumMatchesWon_CROC = Enum.global_NumMatchesWon_CROC;
        public static readonly StatID global_NumMatchesWon_PONG = Enum.global_NumMatchesWon_PONG;
        public static readonly StatID global_NumMatchesWon_BOSS = Enum.global_NumMatchesWon_BOSS;
        public static readonly StatID global_NumMatchesWon_COP = Enum.global_NumMatchesWon_COP;
        public static readonly StatID global_NumMatchesWon_ELECTRO = Enum.global_NumMatchesWon_ELECTRO;
        public static readonly StatID global_NumMatchesWon_SKATE = Enum.global_NumMatchesWon_SKATE;
        public static readonly StatID global_NumMatchesLost_KID = Enum.global_NumMatchesLost_KID;
        public static readonly StatID global_NumMatchesLost_ROBOT = Enum.global_NumMatchesLost_ROBOT;
        public static readonly StatID global_NumMatchesLost_CANDY = Enum.global_NumMatchesLost_CANDY;
        public static readonly StatID global_NumMatchesLost_BOOM = Enum.global_NumMatchesLost_BOOM;
        public static readonly StatID global_NumMatchesLost_CROC = Enum.global_NumMatchesLost_CROC;
        public static readonly StatID global_NumMatchesLost_PONG = Enum.global_NumMatchesLost_PONG;
        public static readonly StatID global_NumMatchesLost_BOSS = Enum.global_NumMatchesLost_BOSS;
        public static readonly StatID global_NumMatchesLost_COP = Enum.global_NumMatchesLost_COP;
        public static readonly StatID global_NumMatchesLost_ELECTRO = Enum.global_NumMatchesLost_ELECTRO;
        public static readonly StatID global_NumMatchesLost_SKATE = Enum.global_NumMatchesLost_SKATE;
        public static readonly StatID lossStreak = Enum.lossStreak;
        public static readonly StatID numMatchesPlayed_GRAF = Enum.numMatchesPlayed_GRAF;
        public static readonly StatID numMatchesWon_GRAF = Enum.numMatchesWon_GRAF;
        public static readonly StatID numMatchesLost_GRAF = Enum.numMatchesLost_GRAF;
        public static readonly StatID global_NumMatchesPlayed_GRAF = Enum.global_NumMatchesPlayed_GRAF;
        public static readonly StatID global_NumMatchesWon_GRAF = Enum.global_NumMatchesWon_GRAF;
        public static readonly StatID global_NumMatchesLost_GRAF = Enum.global_NumMatchesLost_GRAF;
        public static readonly StatID numMatchesPlayed_BAG = Enum.numMatchesPlayed_BAG;
        public static readonly StatID numMatchesWon_BAG = Enum.numMatchesWon_BAG;
        public static readonly StatID numMatchesLost_BAG = Enum.numMatchesLost_BAG;
        public static readonly StatID global_NumMatchesPlayed_BAG = Enum.global_NumMatchesPlayed_BAG;
        public static readonly StatID global_NumMatchesWon_BAG = Enum.global_NumMatchesWon_BAG;
        public static readonly StatID global_NumMatchesLost_BAG = Enum.global_NumMatchesLost_BAG;
        public static readonly StatID max = Enum.max;
        #endregion
#pragma warning restore 1591
    }
}
