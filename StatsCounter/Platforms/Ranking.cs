﻿using System;
namespace StatsCounter.Platforms
{
    public class Ranking
    {
#pragma warning disable 1591
        private readonly IJJKCHBCHOG _ranking;

        public Ranking(IJJKCHBCHOG r)
        {
            this._ranking = r;
        }
        public IJJKCHBCHOG Unwrap() => this._ranking;
        public override bool Equals(object obj) => (obj is IJJKCHBCHOG || obj is Ranking) && Equals((IJJKCHBCHOG)obj, (IJJKCHBCHOG)this);
        public override int GetHashCode() => _ranking.GetHashCode();
        public override string ToString() => _ranking.ToString();
        public static implicit operator IJJKCHBCHOG(Ranking r) => r._ranking;
        public static implicit operator Ranking(IJJKCHBCHOG r) => new Ranking(r);

        #region wrapper
        #endregion wrapper
#pragma warning restore 1591
    }
}
