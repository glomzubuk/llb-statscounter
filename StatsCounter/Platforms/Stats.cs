﻿using System;
using System.Collections.Generic;
using LLBML.Utils;

namespace StatsCounter.Platforms
{
    public class Stats
    {
#pragma warning disable 1591
        private readonly KKAHBLGMGGI _stats;

        public Stats(KKAHBLGMGGI s)
        {
            this._stats = s;
        }
        public KKAHBLGMGGI _ => Unwrap();
        public KKAHBLGMGGI Unwrap() => this._stats;
        public override bool Equals(object obj) => (obj is KKAHBLGMGGI || obj is Stats) && Equals((KKAHBLGMGGI)obj, (KKAHBLGMGGI)this);
        public override int GetHashCode() => _stats.GetHashCode();
        public override string ToString() => _stats.ToString();
        public static implicit operator KKAHBLGMGGI(Stats s) => s._stats;
        public static implicit operator Stats(KKAHBLGMGGI s) => new Stats(s);


        #region wrapper
        public static Stats Instance => CGLLJHHAJAK.JEFNEBAPGHO;
        public bool IsInitialized => _stats.FNAKGIFFMID;

        public Dictionary<string, Dictionary<AKFAKLAWWAE, KKAHBLGMGGI.AKGAKGALALR>> opponentsStatsCache => _stats.CBCDPJJKJHJ;
        public State state => _stats.ACGGIKGFMJP;

        public void ClearCacheStatsForOpponent(string userID) => _stats.ALOJAFLALKM(userID);
        public void DownloadStatsForOpponent(string userID) => _stats.GJGPFIOMNJN(userID);
        public long GetStatValue(StatID stat) => _stats.LHMKKKLJOHD(stat);
        public long GetStatValueForUser(string userID, StatID stat) => _stats.EBGKNMGMPEO(userID, stat);
        #endregion wrapper
#pragma warning restore 1591


        public static bool Initialized => Instance?.IsInitialized ?? false;



        public class State : EnumWrapper<KKAHBLGMGGI.BCODKLJGFND>
        {
#pragma warning disable 1591
            State(int id) : base(id) { }
            State(KKAHBLGMGGI.BCODKLJGFND e) : base((int)e) { }

            public override string ToString() => ((Enum)this.id).ToString();


            public static implicit operator KKAHBLGMGGI.BCODKLJGFND(State ew) => (KKAHBLGMGGI.BCODKLJGFND)ew.id;
            public static implicit operator State(KKAHBLGMGGI.BCODKLJGFND val) => new State(val);
            public static implicit operator State(int id) => new State(id);
            public static implicit operator State(Enum val) => new State((int)val);
            public static implicit operator Enum(State ew) => (Enum)ew.id;

            #region wrapper
            public enum Enum
            {
                none = -1,
                notInitialized,
                initializing,
                ready,
                pullingStat,
                pushingStat
            }

            public enum Enum_Mapping
            {
                none = KKAHBLGMGGI.BCODKLJGFND.DONMMENPJBN,
                notInitialized = KKAHBLGMGGI.BCODKLJGFND.OJBEPCMOBNF,
                initializing = KKAHBLGMGGI.BCODKLJGFND.JJBDKEBJLMM,
                ready = KKAHBLGMGGI.BCODKLJGFND.GFCMODHPPCN,
                pullingStat = KKAHBLGMGGI.BCODKLJGFND.MBLOJAKNJNF,
                pushingStat = KKAHBLGMGGI.BCODKLJGFND.OPKLLLHGFDO,
            }

            public static readonly State none = Enum.none;
            public static readonly State notInitialized = Enum.notInitialized;
            public static readonly State initializing = Enum.initializing;
            public static readonly State ready = Enum.ready;
            public static readonly State pullingStat = Enum.pullingStat;
            public static readonly State pushingStat = Enum.pushingStat;
            #endregion
#pragma warning restore 1591
        }

    }
}
