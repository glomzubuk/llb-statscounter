﻿using System;

namespace StatsCounter.Platforms
{
    public class PlatformBase
    {
#pragma warning disable 1591
        private readonly KIIIINKJKNI _platformBase;

        public PlatformBase(KIIIINKJKNI pb)
        {
            this._platformBase = pb;
        }
        public KIIIINKJKNI Unwrap() => this._platformBase;
        public override bool Equals(object obj) => (obj is KIIIINKJKNI || obj is PlatformBase) && Equals((KIIIINKJKNI)obj, (KIIIINKJKNI)this);
        public override int GetHashCode() => _platformBase.GetHashCode();
        public override string ToString() => _platformBase.ToString();
        public static implicit operator KIIIINKJKNI(PlatformBase pb) => pb._platformBase;
        public static implicit operator PlatformBase(KIIIINKJKNI pb) => new PlatformBase(pb);


        #region wrapper
        #endregion wrapper
#pragma warning restore 1591
    }
}
